'use strict';
module.exports = (sequelize, DataTypes) => {
  var Article = sequelize.define('Article', {
    article_title: DataTypes.STRING,
    article_body: DataTypes.STRING,
  });
  return Article;
};

// Make sure you complete other models fields
