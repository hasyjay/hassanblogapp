var Author = require('../models/author');
var models = require('../models');

// Display author create form on GET.
exports.author_create_get = function(req, res, next) {
        // create author GET controller logic here 
        res.render('forms/author_form', { title: 'Create Author', layout: 'layouts/detail' });
};

// Handle author create on POST.
exports.author_create_post = function(req, res, next) {
        // create author POST controller logic here
        models.Author.create({
                first_name: req.body.first_name,
                last_name: req.body.last_name,
                username: req.body.username,
                role: req.body.role,
                email: req.body.email
        }).then(function() {
                res.redirect('/blog/authors')
        })
};

// Display author delete form on GET.
exports.author_delete_get = function(req, res, next) {
        // GET logic to delete an author here
        if (!req.params.author_id.match(/^[0-9]+$/)) {
                res.render('error/article_error')
        }
        else {
                models.Author.destroy({
                        // find the post_id to delete from database
                        where: {
                                id: req.params.author_id
                        }
                }).then(function() {
                        // If an author gets deleted successfully, we just redirect to posts list
                        // no need to render a page
                        res.redirect('/blog/authors');
                        console.log("Author deleted successfully");
                });
        }



};

// Handle author delete on POST.
exports.author_delete_post = function(req, res, next) {
        // GET logic to delete an author here
        models.Author.destroy({
                // find the post_id to delete from database
                where: {
                        id: req.params.author_id
                }
        }).then(function() {
                // If an author gets deleted successfully, we just redirect to posts list
                // no need to render a page
                res.redirect('/blog/authors');
                console.log("Author deleted successfully");
        });


};

// Display author update form on GET.
exports.author_update_get = function(req, res, next) {
        // GET logic to update an author here
        if (!req.params.author_id.match(/^[0-9]+$/)) {
                res.render('error/article_error')
        }
        else {
                models.Author.findById(
                        req.params.author_id
                ).then(function(author) {
                        // renders a post form
                        res.render('forms/author_form', { title: 'Update Author', author: author, layout: 'layouts/detail' });
                        console.log("Author update get successful");
                });
        }


        // renders author form

};

// Handle post update on POST.
exports.author_update_post = function(req, res, next) {
        // POST logic to update an author here
        models.Author.update({
                        first_name: req.body.first_name,
                        last_name: req.body.last_name,
                        username: req.body.username,
                        role: req.body.role,
                        email: req.body.email
                }, {
                        where: {
                                id: req.params.author_id
                        }
                }

        ).then(function() {
                res.redirect('/blog/authors')
        })

};

// Display list of all authors.
exports.author_list = function(req, res, next) {
        // GET controller logic to list all authors
        models.Author.findAll().then((authors) => {

                res.render('pages/author_list', { title: 'Author List', layout: 'layouts/list', authors: authors });
        })


};

// Display detail page for a specific author.
exports.author_detail = function(req, res, next) {
        // GET controller logic to display just one author
        if (!req.params.author_id.match(/^[0-9]+$/)) {
                res.render('error/article_error')
        }
        else {
                models.Author.findById(
                        req.params.author_id
                ).then(function(author) {
                        // renders an inividual author details page
                        res.render('pages/author_detail', { title: 'Author Details', layout: 'layouts/detail', author: author });

                });
        }

        // renders an individual author details page

};
