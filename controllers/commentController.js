var Comment = require('../models/comment');
var models = require('../models');

// Display comment create form on GET.
exports.comment_create_get = function(req, res, next) {
        // create comment GET controller logic here 

        // renders a comment form
        res.render('forms/comment_form', { title: 'Create Comment', layout: 'layouts/detail' });
};

// Handle comment create on POST.
exports.comment_create_post = function(req, res, next) {
        // create comment POST controller logic here
        models.Comment.create({
                title: req.body.title
        }).then(function() {
                console.log("comment created successfully");
                // check if there was an error during post creation
                res.redirect("/blog/comments");
        });
        // If a comment gets created successfully, we just redirect to comments list
        // no need to render a page

};

// Display comment delete form on GET.
exports.comment_delete_get = function(req, res, next) {
        // GET logic to delete a comment here
        if (!req.params.comment_id.match(/^[0-9]+$/)) {
                res.render('error/article_error')
        }
        else {
                models.Comment.destroy({
                        // find the post_id to delete from database
                        where: {
                                id: req.params.comment_id
                        }
                }).then(function() {
                        // If an comment gets deleted successfully, we just redirect to comments list
                        // no need to render a page
                        res.redirect('/blog/comments');
                        console.log("comment deleted successfully");
                });
        }


};

// Handle comment delete on POST.
exports.comment_delete_post = function(req, res, next) {
        // POST logic to delete a comment here
        models.Comment.destroy({
                // find the comment_id to delete from database
                where: {
                        id: req.params.comment_id
                }
        }).then(function() {
                // If an post gets deleted successfully, we just redirect to comment list
                // no need to render a page
                res.redirect('/blog/comments');
                console.log("comment deleted successfully");
        });

};

// Display comment update form on GET.
exports.comment_update_get = function(req, res, next) {
        // GET logic to update a comment here
        console.log("ID is " + req.params.comment_id);
        if (!req.params.comment_id.match(/^[0-9]+$/)) {
                res.render('error/article_error')
        }
        else {
                models.Comment.findById(
                        req.params.comment_id
                ).then(function(comment) {
                        // renders a comment form
                        res.render('forms/comment_form', { title: 'Update Comment', layout: 'layouts/detail', comment: comment });
                        console.log("Comment update get successful");
                });
        }



};

// Handle comment update on POST.
exports.comment_update_post = function(req, res, next) {
        // POST logic to update a comment here
        console.log("ID is " + req.params.comment_id);
        models.Comment.update(
                // Values to update
                {
                        title: req.body.title
                }, { // Clause
                        where: {
                                id: req.params.comment_id
                        }
                }
                //   returning: true, where: {id: req.params.post_id} 
        ).then(function() {
                // If an comment gets updated successfully, we just redirect to comments list
                // no need to render a page
                res.redirect("/blog/comments");
                console.log("comment updated successfully");
        });

}
// 
// Display list of all comments.
exports.comment_list = function(req, res, next) {
        // controller logic to display all comments
        models.Comment.findAll().then((comments) => {
                // renders a comment list page
                res.render('pages/comment_list', { title: 'Comment List', comments: comments, layout: 'layouts/list' });
        })

};

// Display detail page for a specific comment.


exports.comment_detail = function(req, res) {
        if (!req.params.comment_id.match(/^[0-9]+$/)) {
                res.render('error/article_error')
        }
        else {
                models.Comment.findById(
                        req.params.comment_id
                ).then((comment) => {
                        // renders an inividual category details page
                        res.render('pages/comment_detail', { title: 'Comment Details', comment: comment, layout: 'layouts/detail' });
                })
        }

}
