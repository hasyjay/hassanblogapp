var Article = require('../models/article');
var Author = require('../models/author');
var Comment = require('../models/comment');
var Category = require('../models/category')
var models = require('../models');

// Display post create form on GET.
exports.article_create_get = function(req, res, next) {
    // renders a post form
    res.render('forms/post_form', { title: 'Create Post', layout: 'layouts/detail' });
    console.log("Post form renders successfully");
};

// Handle post create on POST.
exports.article_create_post = function(req, res, next) {

    // create a new post based on the fields in our post model
    // I have create two fields, but it can be more for your model
    models.Article.create({
        article_title: req.body.article_title,
        article_body: req.body.article_body
    }).then(function() {
        console.log("Article created successfully");
        // check if there was an error during post creation
        res.redirect('/blog/articles');
    });
};

// Display post delete form on GET.
exports.article_delete_get = function(req, res, next) {
    if (!req.params.article_id.match(/^[0-9]+$/)) {
        res.render('error/article_error')
    }
    else {
        models.Article.destroy({
            // find the post_id to delete from database
            where: {
                id: req.params.article_id
            }
        }).then(function() {
            // If an post gets deleted successfully, we just redirect to posts list
            // no need to render a page
            res.redirect('/blog/articles');
            console.log("Post deleted successfully");
        });
    }

};

// Handle post delete on POST.
exports.article_delete_post = function(req, res, next) {
    models.Article.destroy({
        // find the post_id to delete from database
        where: {
            id: req.params.article_id
        }
    }).then(function() {
        // If an post gets deleted successfully, we just redirect to posts list
        // no need to render a page
        res.redirect('/blog/articles');
        console.log("Post deleted successfully");
    });

};

// Display post update form on GET.
exports.article_update_get = function(req, res, next) {
    // Find the post you want to update
    console.log("ID is " + req.params.article_id);
    if (!req.params.article_id.match(/^[0-9]+$/)) {
        res.render('error/article_error')
    }
    else {

        models.Article.findById(
            req.params.article_id
        ).then(function(article) {
            // renders a post form
            res.render('forms/post_form', { title: 'Update Post', article: article, layout: 'layouts/detail' });
            console.log("Post update get successful");
        });
    }


};

// Handle post update on POST.
exports.article_update_post = function(req, res, next) {
    console.log("ID is " + req.params.article_id);


    models.Article.update(
        // Values to update
        {
            article_title: req.body.article_title,
            article_body: req.body.article_body
        }, { // Clause
            where: {
                id: req.params.article_id
            }
        }
        //   returning: true, where: {id: req.params.post_id} 
    ).then(function() {
        // If an post gets updated successfully, we just redirect to posts list
        // no need to render a page
        res.redirect("/blog/articles");
        console.log("article updated successfully");
    });


};

// Display detail page for a specific post.
exports.article_detail = function(req, res, next) {
    if (!req.params.article_id.match(/^[0-9]+$/)) {
        res.render('error/article_error')
    }
    else {
        models.Article.findById(
            req.params.article_id
        ).then(function(article) {
            // renders an inividual post details page
            res.render('pages/post_detail', { title: 'Post Details', article: article, layout: 'layouts/detail' });
            console.log("Post deteials renders successfully");
        });
    }

};


// Display list of all posts.
exports.article_list = function(req, res, next) {

    models.Article.findAll().then(function(articles) {
        // renders a post list page
        console.log("rendering post list");
        res.render('pages/post_list', { title: 'Post List', articles: articles, layout: 'layouts/list' });
        console.log("Posts list renders successfully");
    });

};

// This is the blog homepage.
exports.index = function(req, res) {

    // find the count of articles in database
    models.Article.findAndCountAll().then(function(articleCount) {


        // find the count of users in database
        models.Author.findAndCountAll().then(function(authorCount) {

            // find the count of comments in database
            models.Comment.findAndCountAll().then(function(commentCount) {
                // find the count of categories in database
                models.Category.findAndCountAll().then(function(categoryCount) {

                    res.render('pages/index', {
                        title: 'Homepage',
                        articleCount: articleCount,
                        authorCount: authorCount,
                        commentCount: commentCount,
                        categoryCount: categoryCount,
                        layout: 'layouts/main'
                    });

                    // res.render('pages/index_list_sample', { title: 'article Details', layout: 'layouts/list'});
                    // res.render('pages/index_detail_sample', { page: 'Home' , title: 'article Details', layout: 'layouts/detail'});

                })
            })

        })

    })

};
