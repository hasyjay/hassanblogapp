var Category = require('../models/category');
var models = require('../models');

// Display category create form on GET.
exports.category_create_get = function(req, res, next) {
        // create category GET controller logic here 

        // renders a category form
        res.render('forms/category_form', { title: 'Create Category', layout: 'layouts/detail' });
};

// Handle category create on POST.
exports.category_create_post = function(req, res, next) {
        // create category POST controller logic here
        models.Category.create({
                name: req.body.name
        }).then(() => {
                // If a category gets created successfully, we just redirect to categories list
                // no need to render a page
                res.redirect("/blog/categories");
        })

};

// Display category delete form on GET.
exports.category_delete_get = function(req, res, next) {
        // GET logic to delete a category here
        if (!req.params.category_id.match(/^[0-9]+$/)) {
                res.render('error/article_error')
        }
        else {
                models.Category.destroy({
                        // find the post_id to delete from database
                        where: {
                                id: req.params.category_id
                        }
                }).then(function() {
                        // If an post gets deleted successfully, we just redirect to posts list
                        // no need to render a page
                        res.redirect('/blog/categories');
                        console.log("comment deleted successfully");
                });
        }


};

// Handle category delete on POST.
exports.category_delete_post = function(req, res, next) {
        // POST logic to delete a category here
        models.Category.destroy({
                where: {
                        id: req.params.category_id
                }
        }).then((err) => {
                if (err) throw new Error('post was unable to delete')
                res.redirect('/categories');
        })

};

// Display category update form on GET.
exports.category_update_get = function(req, res, next) {
        // GET logic to update a category here
        // Find the post you want to update
        console.log("ID is " + req.params.category_id);
        if (!req.params.category_id.match(/^[0-9]+$/)) {
                res.render('error/article_error')
        }
        else {
                models.Category.findById(
                        req.params.category_id
                ).then(function(category) {
                        // renders a post form
                        res.render('forms/category_form', { title: 'Update Category', layout: 'layouts/detail', category: category });
                        console.log("Post update get successful");
                });
        }

        // renders a category form

};

// Handle category update on POST.
exports.category_update_post = function(req, res, next) {
        // POST logic to update a category here
        console.log("ID is " + req.params.category_id);
        models.Category.update(
                // Values to update
                {
                        name: req.body.name

                }, { // Clause
                        where: {
                                id: req.params.category_id
                        }
                }
                //   returning: true, where: {id: req.params.post_id} 
        ).then(function() {
                // If an comment gets updated successfully, we just redirect to posts list
                // no need to render a page
                res.redirect("/blog/categories");
                console.log("comment updated successfully");
        });

};

// Display list of all categories.
exports.category_list = function(req, res, next) {
        // controller logic to display all categories
        models.Category.findAll().then((categories) => {
                // renders a category list page
                res.render('pages/category_list', { title: 'Category List', categories: categories, layout: 'layouts/list' });
        })

};

// Display detail page for a specific category.
exports.category_detail = function(req, res, next) {
        // constroller logic to display a single category
        if (!req.params.category_id.match(/^[0-9]+$/)) {
                res.render('error/article_error')
        }
        else {
                models.Category.findById(
                        req.params.category_id
                ).then((category) => {
                        // renders an inividual category details page
                        res.render('pages/category_detail', { title: 'Category Details', category: category, layout: 'layouts/detail' });
                })
        }


};
