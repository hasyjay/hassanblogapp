var express = require('express');
var router = express.Router();


// Require our controllers.
var author_controller = require('../controllers/authorController');
var article_controller = require('../controllers/articleController');
var category_controller = require('../controllers/categoryController');
var comment_controller = require('../controllers/commentController');


/// POST ROUTES ///

// GET request for creating a Post. NOTE This must come before routes that display Post (uses id).
router.get('/article/create', article_controller.article_create_get);

// POST request for creating Post.
router.post('/article/create', article_controller.article_create_post);

// GET request to delete Post.
router.get('/article/:article_id/delete', article_controller.article_delete_get);

// POST request to delete Post.
router.post('/article/:article_id/delete', article_controller.article_delete_post);

// GET request to update Post.
router.get('/article/:article_id/update', article_controller.article_update_get);

// POST request to update Post.
router.post('/article/:article_id/update', article_controller.article_update_post);

// GET request for one Post.
router.get('/article/:article_id', article_controller.article_detail);

// GET request for list of all Post.
router.get('/articles', article_controller.article_list);

/// AUTHOR ROUTES ///

// GET request for creating Author. NOTE This must come before route for id (i.e. display author).
router.get('/author/create', author_controller.author_create_get);

// POST request for creating Author.
router.post('/author/create', author_controller.author_create_post);

// GET request to delete Author.
router.get('/author/:author_id/delete', author_controller.author_delete_get);

// POST request to delete Author
router.post('/author/:author_id/delete', author_controller.author_delete_post);

// GET request to update Author.
router.get('/author/:author_id/update', author_controller.author_update_get);

// POST request to update Author.
router.post('/author/:author_id/update', author_controller.author_update_post);

// GET request for one Author.
router.get('/author/:author_id', author_controller.author_detail);

// GET request for list of all Authors.
router.get('/authors', author_controller.author_list);


/// Category ROUTES ///

// GET request for creating a Category. NOTE This must come before route that displays Category (uses id).
router.get('/category/create', category_controller.category_create_get);

// POST request for creating Category.
router.post('/category/create', category_controller.category_create_post);

// GET request to delete Category.
router.get('/category/:category_id/delete', category_controller.category_delete_get);

// POST request to delete Category.
router.post('/category/:category_id/delete', category_controller.category_delete_post);

// GET request to update Category.
router.get('/category/:category_id/update', category_controller.category_update_get);

// POST request to update Category.
router.post('/category/:category_id/update', category_controller.category_update_post);

// GET request for one Category.
router.get('/category/:category_id', category_controller.category_detail);

// GET request for list of all Categories.
router.get('/categories', category_controller.category_list);


/// COMMENT ROUTES ///

// GET request for creating Comment. NOTE This must come before route for id (i.e. display comment).
router.get('/comment/create', comment_controller.comment_create_get);

// POST request for creating Comment.
router.post('/comment/create', comment_controller.comment_create_post);

// GET request to delete Comment.
router.get('/comment/:comment_id/delete', comment_controller.comment_delete_get);

// POST request to delete Comment
router.post('/comment/:comment_id/delete', comment_controller.comment_delete_post);

// GET request to update Comment.
router.get('/comment/:comment_id/update', comment_controller.comment_update_get);

// POST request to update Comment.
router.post('/comment/:comment_id/update', comment_controller.comment_update_post);

// GET request for one Comment.
router.get('/comment/:comment_id', comment_controller.comment_detail);

// GET request for list of all Comments.
router.get('/comments', comment_controller.comment_list);

// GET blog home page.
router.get('/', article_controller.index);

// export all the router created
module.exports = router;
